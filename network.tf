# Key Name "nexus-terraform-keypair" - Should be created in the region beforehand - Check if can be automated

# Create VPC and Subnets
resource "alicloud_vpc" "vpc" {
  name       = "NEXUS_VPC_${var.environment_tag}"
  cidr_block = "${var.cidr_vpc}"
}

resource "alicloud_vswitch" "public_vswitch" {
  cidr_block        = "${var.cidr_vswitch}"
  availability_zone = "${var.availability_zone}"
  vpc_id            = "${alicloud_vpc.vpc.id}"
  name              = "${var.environment_tag}_vswitch"
  depends_on = [
    "alicloud_vpc.vpc",
  ]
}

# Configure Security Groups to allow only 80 & 22
resource "alicloud_security_group" "nexus_sg" {
  name   = "NEXUS_SG_${var.environment_tag}"
  vpc_id = "${alicloud_vpc.vpc.id}"  
}

# Allow all egress
resource "alicloud_security_group_rule" "allow_all_egress" {
  type              = "egress"
  ip_protocol       = "all"
  nic_type          = "intranet"
  port_range        = "-1/-1"
  priority          = 1
  security_group_id = "${alicloud_security_group.nexus_sg.id}"
  cidr_ip           = "0.0.0.0/0"
}

# Allow 22 and 80 on the security group
resource "alicloud_security_group_rule" "allow_port_22_tcp_ingress" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  port_range        = "22/22"
  priority          = 1
  security_group_id = "${alicloud_security_group.nexus_sg.id}"
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "allow_port_8081_tcp_ingress" {
  type              = "ingress"
  ip_protocol       = "tcp"
  nic_type          = "intranet"
  port_range        = "8081/8081"
  priority          = 1
  security_group_id = "${alicloud_security_group.nexus_sg.id}"
  cidr_ip           = "0.0.0.0/0"
}


