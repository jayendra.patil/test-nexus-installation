variable "region" {
  default = "ap-southeast-1"
}

variable "availability_zone" {
  default = "ap-southeast-1c"
}

variable "environment_tag" {
  description = "ENV"
  default     = "PROD"
}

variable "cidr_vpc" {
  description = "CIDR block for Nexus VPC"
  default     = "10.0.0.0/16"
}

variable "cidr_vswitch" {
  description = "CIDR block for vswitch 1" // AWS subnet equivalent
  default     = "10.0.128.0/20"
}

variable "instance_ami" {
  default = "m-t4n636g8ewdnr57grww6"
}

variable "instance_type" {
  default = "ecs.t5-lc1m2.small"
}
