# Configure the Alicloud Provider
provider "alicloud" {
  region     = "${var.region}"
}