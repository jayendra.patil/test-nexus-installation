# test-nexus-installation

## This project is for testing the Nexus installation on AliCloud

### Executing the project

1. Initialize the project `terraform init`
2. Export the access key and secret access key 
`export ALICLOUD_ACCESS_KEY="anaccesskey"`
`export ALICLOUD_SECRET_KEY="asecretkey"`
3. Execute the plan `terraform plan`
4. Execute the plan `terraform apply`

### Current Setup

1. SSH key pair to login into the machine was manually created
2. Setup creates a VPC, Subnet, Security Groups and includes a single ECS instance on Centos7 image
2. For the Nexus repository, external NAS storage has been created
4. Currently could not find NAS creating and monting to be automated. So needs to created and linked to the VPC.
5. The mount point changes if it is relinked with the vpc.


### Nexus Installation

1. Mounting Nas on the Instance
`yum -y install nfs-utils`
`sudo mount -t nfs -o vers=4,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport 7a54d49308-uol86.ap-southeast-1.nas.aliyuncs.com:/ /data`

NOTE - the mount point changes is remounted.

2. Nexus Installation

```bash
cd /tmp
wget https://omnitruck.chef.io/install.sh
chmod 400 install.sh
bash /tmp/install.sh
mkdir -p /opt/chef/chef-repo/.chef
echo '{"run_list":["recipe[nexus_repository_manager]"],"nexus_repository_manager":{"version":"3.15.2-01","nexus_download_sha256":"acde357f5bbc6100eb0d5a4c60a1673d5f1f785e71a36cfa308b8dfa45cf25d0","nexus_data":{"path":"/data/nexus-data"},"properties":{"clustered":false}}}' > /opt/chef/chef-repo/.chef/solo.json
echo "file_cache_path '/tmp'" > /opt/chef/chef-repo/.chef/solo.rb
chmod 400 /opt/chef/chef-repo/.chef/solo.json
chmod 400 /opt/chef/chef-repo/.chef/solo.rb
chef-solo -c /opt/chef/chef-repo/.chef/solo.rb -j /opt/chef/chef-repo/.chef/solo.json --recipe-url https://github.com/sonatype/chef-nexus-repository-manager/releases/download/release-0.5.20190212-155606.d1afdfe/chef-nexus-repository-manager.tar.gz
```