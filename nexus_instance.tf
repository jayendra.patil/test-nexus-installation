# Create an instance for Nexus

resource "alicloud_instance" "nexus_instance" {
  instance_name = "nexus-ecs"

  # Singapore
  security_groups = ["${alicloud_security_group.nexus_sg.id}"]
  vswitch_id      = "${alicloud_vswitch.public_vswitch.id}"

  # instance type
  image_id      = "${var.instance_ami}"
  instance_type = "${var.instance_type}"

  # Assign public ip address
  internet_max_bandwidth_out = 1

  key_name = "nexus-terraform-keypair"

  user_data = "${file("./files/userdata.sh")}"

  tags {
    Name = "nexus_test_terraform"
  }
}
